<li class="menu-header">Starter</li>

<li class="menu-header">Sport Center</li>
@foreach (getSportMenu() as $item)

  @isset($item->sub)
    <li class="dropdown {{ session('active_menu_name') === $item->slug ? 'active' : '' }}">
      <a href="#" class="nav-link has-dropdown"><i
          class="{{ isset($item->logo) ? $item->logo : 'fas fa-ellipsis-h' }}"></i>
        <span>{{ $item->title }}</span></a>
      @if (count($item->sub) > 0)
        <ul class="dropdown-menu">
          @foreach ($item->sub as $sub)
            <li class="{{ session('active_menu_action') === $sub->name ? 'active' : '' }}">
              <a href="{{ route($sub->route, ['sport' => $sub->slug]) }}">{{ $sub->title }}</a>
            </li>
          @endforeach
        </ul>
      @else

      @endif

    </li>
  @else
    <a href="#" class="nav-link has-dropdown"><i class="fas fa-ellipsis-h"></i> <span>{{ $item->title }}</span></a>

  @endisset
@endforeach
