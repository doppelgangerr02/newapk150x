@extends('admin.layout')
@section('content-header')
  <h1>{{ ucwords($sportname) }} Team List</h1>
@endsection

@section('content-body')
  <div class="row">
    <div class="col-12 col-md-6 col-lg-12">
      @if (session('notice'))
        <div class="alert alert-success alert-dismissible show fade">
          <div class="alert-body">
            <button class="close" data-dismiss="alert">
              <span>&times;</span>
            </button>
            {{ session('notice') }}
          </div>
        </div>
      @endif
      <div class="card">
        <div class="card-header">
          <h4></h4>
          <div class="card-header-action">
            <a href="{{ route('admin.sportdata.addMatch', $sportname) }}" class="btn btn-icon btn-primary" href="#"><i
                class="fas fa-plus"></i> ADD</a>
            <button class="btn btn-icon btn-success" id="save-match-position" data-attr="{{ $sportname }}"><i
                class="fas fa-plus"></i> SAVE</button>

          </div>
        </div>
        <div class="card-body">
          <table class="table table-striped table-dark">
            <thead>
              <tr>
                <th scope="col">Liga</th>
                <th scope="col">Team</th>
                <th scope="col" class="text-center">Action</th>
              </tr>
            </thead>
            <tbody id="match-list">
              @foreach (getMatchList($sportname) as $match)
                <tr id="{{ $match->id }}">
                  <td>{{ $match->liga }}</td>
                  <td> {{ $match->homedata->name }} vs {{ $match->awaydata->name }}</td>

                  <td class="text-center">
                    <a href="{{ route('admin.sportdata.editMatch', ['id' => $match->id, 'sport' => $sportname]) }}"
                      class="btn btn-sm btn-icon icon-left btn-primary"><i class="far fa-edit"></i> Edit</a>
                    &nbsp;
                    <a href="{{ route('admin.sportdata.deleteMatch', ['id' => $match->id, 'sport' => $sportname]) }}"
                      class="btn btn-sm btn-icon icon-left btn-danger" id="delete-team" "><i class=" fa fa-times"></i>
                      Delete</a>

                  </td>
                </tr>
              @endforeach

            </tbody>
          </table>

          {{-- <ul class=" list-group"" > --}}

          {{-- @foreach (getMatchList($sportname) as $match) --}}
          {{-- <li class="list-group-item d-flex justify-content-between align-items-center" id="{{ $match->id }}">
              {{ $match->homedata->name }} vs {{ $match->awaydata->name }}
              <span class="badge badge-primary badge-pill">{{ $match->position }}</span>
            </li> --}}

          {{-- <div class="match-data" id="{{ $match->id }}">
                <div class="liga-name">
                  {{ $match->liga }}
                </div>
                <div class="playing-now">
                  <div class="w-5">
                    <img src="{{ $match->homedata->logo }}" alt="" class="lazy team-logo" loading="lazy" width="48"
                      heigh="50">
                    <div class="team-name">{{ $match->homedata->name }}</div>
                  </div>
                  <div class="w-2">
                    vs
                  </div>
                  <div class="w-5">
                    <img src="{{ $match->awaydata->logo }}" alt="" class="lazy team-logo" loading="lazy" width="48"
                      heigh="50">
                    <div class="team-name">{{ $match->awaydata->name }}</div>
                  </div>
                </div>
                <div class="action">
                  <a href="{{ route('admin.sportdata.editMatch', ['id' => $match->id, 'sport' => $sportname]) }}"
                    class="btn btn-sm btn-icon icon-left btn-primary"><i class="far fa-edit"></i> Edit</a>

                  <a href="{{ route('admin.sportdata.deleteMatch', ['id' => $match->id, 'sport' => $sportname]) }}"
                    class="btn btn-sm btn-icon icon-left btn-danger" id="delete-team" "><i class=" fa fa-times"></i>
                    Delete</a>

                </div>

              </div> --}}



          {{-- @endforeach --}}
          {{-- </ul> --}}

          {{-- <div class="row sortable-card">
            <div class="col-12 col-md-6 col-lg-3">
              <div class="card card-primary">
                <div class="card-header">
                  <h4>Card Header</h4>
                </div>
                <div class="card-body">
                  <p>Card <code>.card-primary</code></p>
                </div>
              </div>
            </div>

          </div> --}}
        </div>
      </div>
    </div>
  @endsection


  @push('css')
    <link rel="stylesheet" href="/assets/modules/datatables/datatables.min.css">
    <link rel="stylesheet" href="/assets/modules/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="/assets/modules/datatables/Select-1.2.4/css/select.bootstrap4.min.css">

  @endpush

  @push('js')
    <!-- JS Libraies -->
    <script src="/assets/modules/datatables/datatables.min.js"></script>
    <script src="/assets/modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js"></script>
    <script src="/assets/modules/datatables/Select-1.2.4/js/dataTables.select.min.js"></script>
    <script src="/assets/modules/jquery-ui/jquery-ui.min.js"></script>
    <script src="/assets/modules/jquery-sortable-list/jquery-sortable-lists.min.js"></script>

    <!-- Page Specific JS File -->
    <script>
      $("#team-table").dataTable({
        "columnDefs": [{
          "sortable": true,
          "targets": [0]
        }]
      });

    </script>

  @endpush
