<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Login</title>
</head>

<body>

  <form action="{{ route('do.login') }}" method="post">
    @csrf
    <div class="login-form">
      <h2>LOGIN </h2>
      <div class="email">
        <input type="email" name="email">
      </div>
      <div class="password">
        <input type="password" name="password">
      </div>

      <input type="submit">
    </div>

  </form>
</body>

</html>
