require('./bootstrap');



$(function() {
    // team delete button

    // delete button end

    $("#match-list").sortable({});


    // save-match-position
    $("#save-match-position").on("click", function() {
        var position = $("#match-list").sortable("toArray");
        var sportname = $("#save-match-position").data("attr")
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
            }
        });

        var formData = {
            data: position,
            sportname: sportname
        }
        var ajaxurl = '/api/save-match-position';
        $.ajax({
            type: 'POST',
            url: ajaxurl,
            data: formData,
            dataType: 'json',
            success: function(data) {
                if (data.success == true) {
                    swal('Match Position Updated');
                }
            },
            error: function(data) {
                console.log(data);
            }
        });
    });





});