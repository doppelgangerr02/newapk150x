<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    protected $table = 'teams';
    protected $fillable = [
        'id',
        'name',
        'logo',
        'sport_id',
    ];
    protected $primaryKey = 'id'; // or null
    public $incrementing = false;


    
}
