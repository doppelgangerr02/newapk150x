<?php

use App\Models\SportMatch;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

if (!function_exists('getTeamList')) {
    function getTeamList($sportname)
    {
        return DB::table('teams')->where('sport', $sportname)->get();
    }
}

if (!function_exists('getMatchList')) {
    function getMatchList($sportname)
    {
        $matchList  = SportMatch::with(['homedata', 'awaydata'])->where('sport', $sportname)->orderBy('position', 'asc')->get();

        return $matchList;
        // die;
        return DB::table('match')->where('sport', $sportname)->get();
    }
}



if (!function_exists('saveMatch')) {
    function saveMatch($data)
    {


        try {

            $id = Str::slug($data['home'] . ' vs ' . $data['away']);
            $randomStr = Str::random(7);
            $new =  new SportMatch();
            $new->fill([
                'id' => $randomStr,
                'slug' => $id,
                'liga' => $data['liga'],
                'home' => $data['home'],
                'away' => $data['away'],
                'sport' => $data['sport'],
                'sources' => json_encode($data['sources'])
            ]);
            $new->save();



            $jsonData = SportMatch::with('homedata', 'awaydata')->where('slug', $id)->first();


            // $id = Str::slug($data['home'] . ' vs ' . $data['away']);
            // DB::table('match')->insert([
            //     'id' => Str::uuid(),
            //     'slug' => $id,
            //     'liga' => $data['liga'],
            //     'home' => $data['home'],
            //     'away' => $data['away'],
            //     'sport' => $data['sport'],
            //     'sources' => json_encode($data['sources'])
            // ]);

            saveJson($randomStr, $jsonData);


            return true;
        } catch (\Throwable $th) {
            // dd($th);
            Log::error("Insert Match failed, msg : " . $th->getMessage());
            return false;
        }
    }
}


if (!function_exists('saveJson')) {
    function saveJson($filename, $data)
    {
        file_put_contents(public_path() . '/json/' . $filename . '.json', json_encode($data));
    }
}



if (!function_exists('getTeam')) {
    function getTeam($id, $sport)
    {
        try {
            $team = DB::table('teams')
                ->where('id', $id)
                ->where('sport', $sport)
                ->first();
            return $team;
        } catch (\Throwable $th) {
            Log::error("Get team info failed, msg : " . $th->getMessage());
            return false;
        }
    }
}

if (!function_exists('destroyTeam')) {
    function destroyTeam($id, $sport)
    {
        try {
            $destroy = DB::table('teams')
                ->where([
                    'id' => $id,
                    'sport' => $sport
                ])
                ->delete();
            return true;
        } catch (\Throwable $th) {
            Log::error("Delete team failed, msg : " . $th->getMessage());
            return false;
        }
    }
}
if (!function_exists('saveSport')) {

    function saveSport($sport)
    {
        // dd($sport);
        $data = getMatchList($sport);
        saveJson($sport, $data);
    }
}
if (!function_exists('destroyMatch')) {
    function destroyMatch($id, $sport)
    {
        try {
            $destroy = DB::table('match')
                ->where([
                    'id' => $id,
                    'sport' => $sport
                ])
                ->delete();


            saveSport($sport);
            $file = public_path() . '/json/' . $id . '.json';
            if (file_exists($file)) {
                unlink($file);
            }
            return true;
        } catch (\Throwable $th) {
            Log::error("Delete team failed, msg : " . $th->getMessage());
            return false;
        }
    }
}


if (!function_exists('addTeam')) {

    function addTeam($data)
    {

        try {
            DB::table('teams')
                ->insert([
                    'id' => Str::slug($data['name']),
                    'name' => $data['name'],
                    'logo' => $data['logo'],
                    'sport' => $data['sport']
                ]);
            return true;
        } catch (\Throwable $th) {
            Log::error("Add team error, info : " . $th->getMessage());
            return false;
        }
    }
}

if (!function_exists('updateTeam')) {
    function updateTeam($data)
    {
        try {
            DB::table('teams')
                ->where('id', $data['id'])
                ->where('sport', $data['sport'])
                ->update(
                    [
                        'name' => $data['name'],
                        'logo' => $data['logo'],
                    ]
                );
            return true;
        } catch (\Throwable $th) {
            Log::error("Save team error, info : " . $th->getMessage());
            return false;
        }
    }
}




if (!function_exists('getSportMenu')) {
    function getSportMenu()
    {

        /**
         * 
         */
        $menuList = array(
            [
                'title' => "Football",
                'slug' => "football",
                "route" => "admin.sportdata",
                'logo' => 'fas fa-futbol',
                "sub" => array(
                    [
                        'name' => "indexTeam",
                        'title' => "Team",
                        'slug' => "football",
                        'logo' => 'fas fa-users',
                        "route" => "admin.sportdata.listTeam",
                    ],
                    [
                        'name' => "indexMatch",
                        'title' => "Match",
                        'slug' => "football",
                        'logo' => 'fas fa-calendar-alt',
                        "route" => "admin.sportdata.listMatch",
                    ],


                )
            ],
            [
                'title' => "Basketball",
                'slug' => "basketball",
                "route" => "admin.sportdata",
                'logo' => 'fas fa-basketball-ball',
                "sub" => array(
                    [
                        'name' => "indexTeam",
                        'title' => "Team",
                        'slug' => "basketball",
                        "route" => "admin.sportdata.listTeam",
                    ],
                    [
                        'name' => "indexMatch",
                        'title' => "Match",
                        'slug' => "basketball",
                        "route" => "admin.sportdata.listMatch",
                    ],


                )
            ],
            [
                'title' => "Baseball",
                'slug' => "baseball",
                "route" => "admin.sportdata",
                'logo' => 'fas fa-baseball-ball',
                "sub" => array(
                    [
                        'name' => "indexTeam",
                        'title' => "Team",
                        'slug' => "baseball",
                        "route" => "admin.sportdata.listTeam",
                    ],
                    [
                        'name' => "indexMatch",
                        'title' => "Match",
                        'slug' => "baseball",
                        "route" => "admin.sportdata.listMatch",
                    ],
                )
            ],
            [
                'title' => "Tennis",
                'slug' => "tennis",
                "route" => "admin.sportdata",
                'logo' => 'fas fa-tv',
                "sub" => array(
                    [
                        'name' => "indexTeam",
                        'title' => "Team",
                        'slug' => "tennis",
                        "route" => "admin.sportdata.listTeam",
                    ],
                    [
                        'name' => "indexMatch",
                        'title' => "Match",
                        'slug' => "tennis",
                        "route" => "admin.sportdata.listMatch",
                    ],
                )
            ],
            [
                'title' => "UFC",
                'slug' => "ufc",
                "route" => "admin.football",
                'logo' => 'fas fa-tv',
                "sub" => array(
                    [
                        'name' => "indexTeam",
                        'title' => "Team",
                        'slug' => "ufc",
                        "route" => "admin.sportdata.listTeam",
                    ],
                    [
                        'name' => "indexMatch",
                        'title' => "Match",
                        'slug' => "ufc",
                        "route" => "admin.sportdata.listMatch",
                    ],

                )
            ],
            [
                'title' => "Badminton",
                'slug' => "badminton",
                "route" => "admin.sportdata",
                'logo' => 'fas fa-tv',
                "sub" => array(
                    [
                        'name' => "indexTeam",
                        'title' => "Team",
                        'slug' => "badminton",
                        "route" => "admin.sportdata.listTeam",
                    ],
                    [
                        'name' => "indexMatch",
                        'title' => "Match",
                        'slug' => "badminton",
                        "route" => "admin.sportdata.listMatch",
                    ],

                )
            ],

        );


        return json_decode(json_encode($menuList));
    }
}

if (!function_exists('setActiveMenu')) {

    function setActiveMenu($name, $action)
    {
        session()->put('active_menu_name', $name);
        session()->put('active_menu_action', $action);
    }
}
