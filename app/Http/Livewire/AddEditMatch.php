<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Psy\CodeCleaner\UseStatementPass;

class AddEditMatch extends Component
{
    public $sportname;
    public $editData;
    public $streamList = [];

    public $home = "";
    public $away = "";
    public $liga = "";

    public $homeError = null;
    public $awayError = null;
    public $ligaError = null;


    public $saveData;



    public function mount()
    {
        $this->editData = [
            'liga' => "",
            'home' => "",
            'away' => "",
        ];
    }



    public function save()
    {
        if ($this->liga === "") {
            $this->ligaError =  "Input Liga Terlebih Dahulu";
        } else {
            $this->ligaError = null;
        }

        if ($this->away === "") {
            $this->awayError =  "Pilih Away Team Terlebih Dahulu";
        } else {
            $this->awayError = null;
        }

        if ($this->home === "") {
            $this->homeError =  "Pilih Home Team Terlebih Dahulu";
        } else {
            $this->homeError = null;
        }

        $this->saveData = [
            'liga' => $this->liga,
            'home' => $this->home,
            'away' => $this->away,
            'sport' => $this->sportname,
            'sources' => $this->streamList
        ];


        if ($this->ligaError === null && $this->homeError === null && $this->awayError === null) {

            $save =  saveMatch($this->saveData);
            saveSport($this->sportname);
            if ($save === true) {
                return redirect(route('admin.sportdata.listMatch', ['sport' => $this->sportname]));
            }
        }
    }
    public function addStream()
    {
        array_push($this->streamList, [
            'type' => "HLS",
            "source" => ""
        ]);
    }

    public function removeSource($key)
    {
        unset($this->streamList[$key]);
        $this->streamList = array_values($this->streamList);
    }
    public function render()
    {

        $teamList = getTeamList($this->sportname);

        return view('livewire.add-edit-match', [

            'teamList' => $teamList
        ]);
    }
}
