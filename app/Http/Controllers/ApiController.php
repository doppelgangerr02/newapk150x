<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;


use Illuminate\Http\Request;

class ApiController extends Controller
{
    public function changeMatchPosition(Request $request)
    {
        try {
            foreach ($request->data as $key => $value) {
                DB::table('match')->where('id', $value)->update(['position' => $key + 1]);
            }

            saveSport($request->sportname);
            return response([
                'success' => true,
            ], 200);
        } catch (\Throwable $th) {
            return response([
                'success' => false,
            ], 200);
        }
    }
}
