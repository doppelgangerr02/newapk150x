<?php

use App\Http\Controllers\AdminpanelController;
use App\Http\Controllers\AuthxController;
use App\Http\Controllers\MatchController;
use App\Http\Controllers\TeamController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::middleware(['admin'])->group(function () {
    Route::prefix('panel')->group(function () {
        Route::get('/', [AdminpanelController::class, 'index'])->name('admin.index');

        route::get('{sport?}/add-match', [MatchController::class, 'addMatch'])->name('admin.sportdata.addMatch');
        route::get('{sport?}/edit-match/{id?}', [MatchController::class, 'editMatch'])->name('admin.sportdata.editMatch');
        route::post('/match-action', [MatchController::class, 'doEditMatch'])->name('admin.sportdata.doEditMatch');
        route::get('{sport?}/list-match', [MatchController::class, 'listMatch'])->name('admin.sportdata.listMatch');

        route::get('{sport?}/delete-match/{id?}', [MatchController::class, 'deleteMatch'])->name('admin.sportdata.deleteMatch');
        route::post('{sport?}/delete-match/{id?}', [MatchController::class, 'doDeleteMatch'])->name('admin.sportdata.doDeleteMatch');

        route::get('{sport?}/add-team', [TeamController::class, 'addTeam'])->name('admin.sportdata.addteam');


        // team
        route::get('{sport?}/edit-team/{id?}', [TeamController::class, 'editTeam'])->name('admin.sportdata.editTeam');

        route::get('{sport?}/delete-team/{id?}', [TeamController::class, 'deleteTeam'])->name('admin.sportdata.deleteTeam');
        route::post('{sport?}/delete-team/{id?}', [TeamController::class, 'doDeleteTeam'])->name('admin.sportdata.doDeleteTeam');
        route::post('/team-action', [TeamController::class, 'doEditTeam'])->name('admin.sportdata.doEditTeam');

        route::get('{sport?}/list-team', [TeamController::class, 'listTeam'])->name('admin.sportdata.listTeam');
        // route::get('{sport?}', [TeamController::class, 'index'])->name('admin.sportdata.teamList');



        // sports section
        Route::prefix('sports')->group(function () {
        });
        // blocksection
        Route::prefix('block')->group(function () {
        });

        // tipster section
        Route::prefix('tispter')->group(function () {
        });
    });
});



Route::prefix('auth')->group(function () {
    Route::get('login', [AuthxController::class, 'login'])->name('show.login');
    Route::post('login', [AuthxController::class, 'doLogin'])->name('do.login');
    Route::any('logout', [AuthxController::class, 'logout'])->name('do.logout');
});
